import React from "react";

export default function SignupForm() {
  const register = (e) => {
    e.preventDefault();
    const form = document.querySelectorAll("input");
    for (let i = 0; i < form.length; i++) {
      if (form[i].value == 0) {
        alert("Please enter all the fields");
        return;
      }
      console.log(form[i].value);
    }
  };
  return (
    <div className="form-container">
      <div className="form-body">
        <div>
          <h1>Sign up Registration Form</h1>
        </div>
        <form id="form-div">
          <label className="label">First Name : </label>
          <input className="input-field" type="text" />

          <label className="label">Last Name : </label>
          <input className="input-field" type="text" />

          <label className="label">Gender : </label>
          <select>
            <option value="Select">Select</option>
            <option value="Male">Male</option>
            <option value="Female">Female</option>
            <option value="Others">others</option>
          </select>

          <label className="label">Age</label>
          <input className="input-field" type="number" />

          <label className="label">Email id : </label>
          <input className="input-field" type="email" />

          <label className="label">Password : </label>
          <input className="input-field" type="text" />

          <label className="label">Address : </label>
          <input className="input-field" type="text" />

          <button className="btn" onClick={register}>
            Submit
          </button>
        </form>
      </div>
    </div>
  );
}
