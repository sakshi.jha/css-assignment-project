import "./components/style.css";

import SignupForm from "./components/Form";

function App() {
  return (
    <div className="App">
      <SignupForm />
    </div>
  );
}

export default App;
