let btn = document.getElementsByClassName("search-icon")[0];
let search = document.getElementsByClassName("search")[0];
console.log(btn);
console.log(search);
btn.addEventListener("click", () => {
  search.classList.toggle("search-expand");
});

let form = document.getElementById("formId");

form.addEventListener("click", youClicked);

function youClicked(e) {
  e.preventDefault();
  let value = document.getElementById("value").value;
  if (value == "") {
    alert("ENTER VALID VALUE");
  } else {
    document.getElementById(
      "item"
    ).innerHTML += `<li class="itemContent">${value}<button class="but">X</button></li>`;
  }
}

let del = document.getElementById("item");

del.addEventListener("click", remove);

function remove(e) {
  if (e.target.classList.contains("but")) {
    if (confirm("Are you sure ?")) {
      var delParent = e.target.parentElement;
      del.removeChild(delParent);
    }
  }
}
