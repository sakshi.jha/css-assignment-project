// fetching select values
let selectGroup = document.getElementById("select-group").options;
// console.log(selectGroup[0].value);
let selectGroup2 = document.getElementById("select-group2");

//Fetching buttton for the event listening
let addBtn = document.getElementsByClassName("add-btn")[0];

// fetching parent div for the input element
let parentDiv = document.getElementById("updatedForm");
console.log(parentDiv);
// Creating new elements

function createInputs(type) {
  let newDiv = document.createElement("div");
  newDiv.setAttribute("class", "input-label");
  newDiv.innerHTML = `<input type="${type}" >`;
  return newDiv;
}

// Add event listener for clicking the add button
addBtn.addEventListener("click", (e) => {
  e.preventDefault();
  getInputElements();
});

function getInputElements() {
  for (let i = 0; i < selectGroup.length; i++) {
    if (selectGroup[i].selected === true) {
      let newDiv = createInputs(selectGroup[i].value);
      parentDiv.appendChild(newDiv);
    }
  }
}
