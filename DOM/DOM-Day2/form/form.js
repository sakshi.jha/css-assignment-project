let myInputs = [
  {
    name: "Fullname",
    type: "text",
    placeholder: "Enter the fullname",
    require: true,
  },
  {
    name: "RollNo.",
    type: "text",
    placeholder: "Enter your RollNo",
    require: false,
  },
  {
    name: "Emailid",
    type: "text",
    placeholder: "Enter your emailid",
    require: true,
  },
  {
    name: "MobileNo.",
    type: "text",
    placeholder: "Enter your phone number",
    require: true,
  },
];

// Make a Form
let formDiv = document.createElement("form");
let parentname = document.getElementsByClassName("box")[0];
parentname.appendChild(formDiv);

// Iterating the input values
myInputs.forEach((element) => {
  let input = document.createElement("input");
  input.name = element.name;
  input.type = element.type;
  input.placeholder = element.placeholder;
  input.required = element.require;
  formDiv.appendChild(input);
  console.log(myInputs);
});

// Creating the Submit button
let sub = document.createElement("input");
sub.setAttribute("type", "submit");
sub.setAttribute("value", "SUBMIT");
formDiv.appendChild(sub);
sub.style = "width:120px;margin-left:150px;text-items:center;border-radius:8px";
