//Reverse each word of a given sentence
//let s = "";
function reverseword(str) {
  let s = "";
  for (let i = str.length - 1; i >= 0; i--) {
    s += str[i];
  }
  let splitstr = s.split(" ");
  let start = 0;
  let end = splitstr.length - 1;
  count = 0;
  while (start < end) {
    let temp = splitstr[start];
    splitstr[start] = splitstr[end];
    splitstr[end] = temp;
    start++;
    end--;
  }
  console.log(splitstr.join(" "));
}
reverseword("My name is Sakshi");
