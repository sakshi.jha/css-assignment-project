//Check if the given two strings are anagrams of each other or not.
//For e.g. ‘care’ and ‘race’

function anagram(str1, str2) {
  let obj1 = {};
  let obj2 = {};
  if (str1.length != str2.length) {
    return false;
  }
  for (let i of str1) {
    if (obj1[i] == null) {
      obj1[i] = 1;
    } else {
      obj1[i] += 1;
    }
  }
  for (let i of str2) {
    if (obj2[i] == null) {
      obj2[i] = 1;
    } else {
      obj2[i] += 1;
    }
  }
  for (let i = 0; i < str1.length; i++) {
    if (obj1[str1[i]] != obj2[str1[i]]) {
      return false;
    }
  }
  return true;
}

let str1 = "care";
let str2 = "race";
console.log(anagram(str1, str2));
