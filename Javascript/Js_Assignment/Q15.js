// Sort an array which consists of elements: 0, 1 & 2
let zero = 0,
  first = 0,
  second = 0;
let temp = [];
function sortArr(arr) {
  // Count the number of 0s, 1s and 2s in the array
  for (i = 0; i < arr.length; i++) {
    if (arr[i] == 0) {
      zero++;
    } else if (arr[i] == 1) {
      first++;
    } else {
      second++;
    }
  }
  for (let i = 0; i < zero; i++) {
    temp[i] = 0;
  }
  for (let i = zero; i < zero + first; i++) {
    temp[i] = 1;
  }
  for (let i = zero + first; i < arr.length; i++) {
    temp[i] = 2;
  }
  console.log(temp);
}
// Function calling
let arr = [1, 2, 0, 0, 1, 2, 2, 0];
sortArr(arr);
