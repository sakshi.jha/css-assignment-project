//Calculate the sum of odd numbers greater than 10 and multiples of 3 from a given array

function sumofodd(arr) {
  let sum = 0;
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] % 2 != 0) {
      if (arr[i] > 10 && arr[i] % 3 == 0) {
        sum += arr[i];
      }
    }
  }
  console.log(sum);
}
let arr = [9, 11, 12, 13, 14, 15];
sumofodd(arr);
