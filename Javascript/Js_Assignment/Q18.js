//Given a binary string, convert it into the equivalent decimal number.

function binaryToDecimal(n) {
  let dec = 0;
  let base = 1;
  let temp = n;
  while (temp) {
    let last_digit = temp % 10;
    temp = Math.floor(temp / 10);
    dec += last_digit * base;
    base = base * 2;
  }
  console.log(dec);
}
binaryToDecimal(1011);
