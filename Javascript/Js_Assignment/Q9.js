//Find the missing number in an array of length n, having elements from 1 to n
function missingno(arr) {
  let n = arr.length + 1;
  let total = (n * (n + 1)) / 2;
  let sum = 0;
  for (let i = 0; i < arr.length; i++) {
    sum = sum + arr[i];
  }
  console.log(total - sum);
}
let arr = [1, 2, 3, 5];
missingno(arr);
