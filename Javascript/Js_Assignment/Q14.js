//Create a function that returns base-2 (binary) representation of a base-10 ( decimal ) string number.
//E.g. binary(5) —> 101

function decimaltobinary(n) {
  let rem;
  let sum = "";
  while (n > 0) {
    rem = n % 2;
    sum = sum + rem;
    n = Math.floor(n / 2);
  }
  console.log(sum);
}
decimaltobinary(5);
