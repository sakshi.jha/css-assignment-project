//Find the even duplicate elements in an array of numbers

function evenduplicate(arr) {
  let arr1 = [0, 0, 0, 0, 0, 0, 0];
  for (let i = 0; i < arr.length; i++) {
    arr1[arr[i]]++;
  }
  for (let i = 0; i < arr1.length; i++) {
    if (arr1[i] >= 2) {
      if (i % 2 == 0) {
        console.log(i);
      }
    }
  }
}
let arr = [1, 2, 3, 4, 3, 5, 2, 4];
evenduplicate(arr);
