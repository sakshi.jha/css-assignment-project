//Check if the given string contains a digit

function containdigit(str) {
  for (let i = 0; i < str.length; i++) {
    if (str[i] > 0 && str[i] < 9) {
      return true;
    }
  }
  return false;
}
let str = "sakshi123";
console.log(containdigit(str));
