// Print all even numbers from 10 - 100

function even() {
  for (let i = 10; i <= 100; i++) {
    if (i % 2 == 0) {
      console.log(i);
    }
  }
}
even();
