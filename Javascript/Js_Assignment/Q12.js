//Find the element having the highest frequency in an array of numbers

function maxFreq(arr, n) {
  //using moore's voting algorithm
  let res = 0;
  let count = 1;
  for (let i = 1; i < n; i++) {
    if (arr[i] == arr[res]) {
      count++;
    } else {
      count--;
    }
    if (count == 0) {
      res = i;
      count = 1;
    }
  }
  console.log(arr[res]);
}
let arr = [1, 2, 1, 1, 2, 3, 4, 1];
let n = arr.length;
let freq = maxFreq(arr, n);
let count = 0;
for (let i = 0; i < n; i++) {
  if (arr[i] === freq) {
    count++;
  }
}
