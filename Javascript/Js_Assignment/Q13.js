//A vehicle needs 10 times the amount of fuel than the distance it travels. However, it must always carry a minimum of 100 fuel before setting off. Create a function which calculates the amount of fuel it needs, given the distance.
//E.g. caculateFuel(15)  —> 150

function fuel(a) {
  let res;
  res = a * 10;
  if (res > 100) {
    console.log(res);
  } else {
    console.log(100);
  }
}
fuel(15);
