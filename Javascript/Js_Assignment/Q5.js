//Create a function that reverses an array
function reverse(arr) {
  let arr1 = [];
  let j = 0;
  for (let i = arr.length - 1; i >= 0; i--) {
    arr1[j] = arr[i];
    j++;
  }
  console.log(arr1);
}
let arr = [1, 2, 3, 4, 5];
reverse(arr);
