const fs = require("fs").promises;
let data = async () => {
  let jsonString = await fs.readFile("./info.json", "utf8");
  return JSON.parse(jsonString);
};
//Sign Up

const signUp = async (element) => {
  let newData = await data();
  newData.push(element);
  fs.writeFile("./Signup-data.json", JSON.stringify(newData), "utf8");
};

module.exports = { signUp };
