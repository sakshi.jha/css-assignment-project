// SignIn: It takes 2 params: (emailId and password) and checks if this user exists in our file or not.
// It throws an error if user does not exist in our file
// Returns the user data if it exists in our fie

const { emit } = require("process");

const fs = require("fs").promises;
let data = async () => {
  let jsonString = await fs.readFile("./info.json", "utf8");
  return JSON.parse(jsonString);
};
//Sign In
const signIn = async (email, password) => {
  try {
    newdata = await data();

    let filteredData = newdata.filter((element) => {
      if (element.emailId === email && element.password === password) {
        return element;
      }
    });
    if (filteredData.length !== 0) {
      console.log(filteredData);
    } else {
      throw Error("Please enter valid emailId and password");
    }
  } catch (err) {
    console.log(err);
  }
};

module.exports = { signIn };
