// updateProfile: It takes 3 params: ( emailId, age and address ) and
// updates the age and address of the user with a given emailId.
// It throws an error if user does not exist in our file
// Returns the updated data if user exists

// Update data
const fs = require("fs").promises;
let data = async () => {
  let jsonString = await fs.readFile("./info.json", "utf8");
  return JSON.parse(jsonString);
};
const updateData = async (email, age, Address) => {
  try {
    newdata = await data();
    let filteredData = newdata.filter((element) => {
      if (element.emailId === email) {
        element.age = age;
        element.Address = Address;
        return element;
      }
    });
    console.log(filteredData);
  } catch (err) {
    console.log(err);
  }
};

module.exports = { updateData };
