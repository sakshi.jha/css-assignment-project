// deleteUser: It takes 1 param ( emailId ) and deletes the user from our file.

// Delete data
const fs = require("fs").promises;
let data = async () => {
  let jsonString = await fs.readFile("./info.json", "utf8");
  return JSON.parse(jsonString);
};
const deleteData = async (email) => {
  try {
    newdata = await data();
    let filteredData = newdata.filter((element) => {
      if (element.emailId !== email) {
        return element;
      }
    });

    console.log(filteredData);
  } catch (err) {
    console.log(err);
  }
};

module.exports = { deleteData };
