//Given an array of numbers and sum, find the pairs of elements in the array having the sum.
//E.g. [1, 9, 2, 5, 3] and 5 ===> (2,3)

let temp = [];
let arr = [1, 2, 8, 3, 7];
let target = 5;
let k = 0;
function pairelement(arr, target) {
  for (let i = 0; i < arr.length; i++) {
    for (let j = i + 1; j < arr.length; j++) {
      if (arr[i] + arr[j] == target) {
        temp[k] = [arr[i], arr[j]];
        k++;
      }
    }
  }
  return temp;
}
console.log(pairelement(arr, target));
