//Given an array of person objects, fetch only the objects having age geaer than 25 and capitalize their firstName attribute.

let arr = [
  {
    firstName: "Ravi",
    lastName: "Kumar",
    age: 27,
  },
  {
    firstName: "Amit",
    lastName: "Kumar",
    age: 22,
  },
  {
    firstName: "Ritu",
    lastName: "Kumari",
    age: 30,
  },
  {
    firstName: "Sakshi",
    lastName: "Jha",
    age: 22,
  },
  {
    firstName: "Mayur",
    lastName: "Kumar",
    age: 28,
  },
];
const info = arr.filter((element) => {
  if (element.age > 25) {
    element.firstName = element.firstName.toUpperCase();
    return element;
  }
});
console.log(info);
