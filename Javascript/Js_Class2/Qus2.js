//Sort a given array of person objects by their ‘firstName’.

let arr = [
  {
    firstName: "Ravi",
    lastName: "Kumar",
    age: 27,
  },
  {
    firstName: "Amit",
    lastName: "Kumar",
    age: 22,
  },
  {
    firstName: "Ritu",
    lastName: "Kumari",
    age: 30,
  },
  {
    firstName: "Sakshi",
    lastName: "Jha",
    age: 22,
  },
  {
    firstName: "Mayur",
    lastName: "Kumar",
    age: 28,
  },
];
function compare(a, b) {
  if (a.firstName < b.firstName) {
    return -1;
  }
  if (a.firstName > b.firstName) {
    return 1;
  }
}

console.log(arr.sort(compare));
