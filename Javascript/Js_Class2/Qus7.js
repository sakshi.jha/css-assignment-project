//Given an array of numbers, in which value of the element is equal to the number of positions you can jump. Calculate the minimum number of jumps required to reach the end of the array.
//E.g. [1,3,4,8,2] outputs 2
function minJumps(arr, n) {
  if (n <= 1) return 0;
  if (arr[0] == 0) return -1;
  let maxReach = arr[0];
  let step = arr[0];
  let jump = 1;
  for (i = 1; i < n; i++) {
    if (i == n - 1) return jump;
    maxReach = Math.max(maxReach, i + arr[i]);
    step--;
    if (step == 0) {
      jump++;
      if (i >= maxReach) return -1;
      step = maxReach - i;
    }
  }
  return -1;
}
let arr = [1, 3, 4, 8, 2];
let n = arr.length;
console.log(minJumps(arr, n));
