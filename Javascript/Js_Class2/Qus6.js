//Group array of strings by first letter i.e. create subarrays having words which start with the same letter.

function groupstring(arr) {
  return arr.reduce((store, word) => {
    const letter = word.charAt(0);
    const obj = store[letter] || (store[letter] = []);
    obj.push(word);

    return store;
  }, {});
}

const str = groupstring([
  "sakshi",
  "ankit",
  "rohit",
  "akshat",
  "abhishek",
  "sumon",
  "rahul",
  "kajal",
]);
console.log(str);
