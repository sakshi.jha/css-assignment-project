const fs = require("fs");

async function function1() {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(2);
    }, 2000);
  });
}

async function function2() {
  const result1 = await function1();
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(result1 + 3);
    }, 2000);
  });
}

async function function3() {
  const result2 = await function2();
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(result2 + 4);
    }, 2000);
  });
}

async function function4() {
  const result3 = await function3();
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(result3 + 5);
    }, 2000);
  });
}

let promise = function4();
promise.then((sum) => {
  console.log(sum);
});
