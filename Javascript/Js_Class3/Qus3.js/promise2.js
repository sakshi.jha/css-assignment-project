let promise = new Promise(function (resolve, reject) {
  setTimeout(() => resolve(2), 2000);
});

promise
  .then(function (result) {
    return new Promise((resolve, reject) => {
      setTimeout(() => resolve(result + 3), 2000);
    });
  })
  .then(function (result) {
    return new Promise((resolve, reject) => {
      setTimeout(() => resolve(result + 4), 2000);
    });
  })
  .then(function (result) {
    return new Promise((resolve, reject) => {
      setTimeout(() => resolve(result + 5), 2000);
    });
  })
  .then((result) => {
    console.log(result);
  });
