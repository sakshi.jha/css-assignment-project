// Create 4 json files, utilize all 4 promise methods namely:
// Promise.all, promise.allSettled, promise.any, promise.race
// to read these files in parallel.

const fs = require("fs").promises;

// Student data
function readStudentData() {
  return new Promise((resolve, reject) => {
    let jsonString = fs.readFile("./data/student.json", "utf-8");
    resolve(jsonString);
  });
}
let studentPromise = readStudentData();

// Fruits data
function readFruitsData() {
  return new Promise((resolve, reject) => {
    let jsonString = fs.readFile("./data/fruits.json", "utf-8");
    resolve(jsonString);
  });
}
let fruitsPromise = readFruitsData();

// Employee data
function readEmployeeData() {
  return new Promise((resolve, reject) => {
    let jsonString = fs.readFile("./data/employee.json", "utf-8");
    resolve(jsonString);
  });
}
let employeePromise = readEmployeeData();

// Travel data
function readTravelData() {
  return new Promise((resolve, reject) => {
    let jsonString = fs.readFile("./data/Travel.json", "utf-8");
    resolve(jsonString);
  });
}
let travelPromise = readTravelData();

// Promise.all()
Promise.all([studentPromise, fruitsPromise, employeePromise, travelPromise])
  .then((data) => {
    console.log(data);
  })
  .then((err) => {
    console.log(err);
  });

//Promise.allSettled
Promise.allSettled([
  [studentPromise, fruitsPromise, employeePromise, travelPromise],
])
  .then((data) => {
    console.log(data);
  })
  .catch((err) => console.log(err));

//Promise.any()
Promise.any([[studentPromise, fruitsPromise, employeePromise, travelPromise]])
  .then((data) => {
    console.log(data);
  })
  .catch((err) => console.log(err));

// Promise.race()
Promise.race([[studentPromise, fruitsPromise, employeePromise, travelPromise]])
  .then((data) => {
    console.log(data);
  })
  .catch((err) => console.log(err));
