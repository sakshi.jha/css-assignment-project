let fs = require("fs");

fs.readFile("person.json", (encoding = "utf8"), (error, data) => {
  if (error) {
    console.log("An error occurred");
  }
  data = JSON.parse(data);
  newdata = data.filter((element) => {
    if (element.age > 25) {
      element.firstName = element.firstName.toUpperCase();
      element.lastName = element.lastName.toUpperCase();
      return element;
    }
  });

  console.log(newdata);
  fs.writeFile("new-callback.json", JSON.stringify(newdata), (err) => {
    if (err) {
      console.log("An error has occurred ", err);
    }
    console.log("JSON Updated");
  });
});
