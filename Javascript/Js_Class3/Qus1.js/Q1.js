const fs = require("fs");

//Asynchronous Method

let asyncdata = fs.readFile("person.json", "utf8", function (err, data) {
  //console.log(data);
  return data;
});
//Synchronous Method
const promise1 = () => {
  let syncdata = fs.readFileSync("person.json", "utf8", function (err, data) {
    return data;
  });

  return JSON.parse(syncdata);
};
let data = promise1();
let filterobj = data.filter((element) => {
  if (element.age > 25) {
    element.firstName = element.firstName.toUpperCase();
    element.lastName = element.lastName.toUpperCase();
    return element;
  }
});
console.log(filterobj);
fs.writeFileSync("./copy.json", JSON.stringify(filterobj));
