const { json } = require("stream/consumers");

let fs = require("fs").promises;
const fn = async () => {
  const ru = await fs.readFile("person.json", "utf8");
  return JSON.parse(ru);
};
const maindata = async () => {
  let data = await fn();
  let filterobj = data.filter((element) => {
    if (element.age > 25) {
      element.firstName = element.firstName.toUpperCase();
      element.lastName = element.lastName.toUpperCase();
      return element;
    }
  });
  console.log(filterobj);
};
maindata();
