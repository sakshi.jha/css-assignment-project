let fs = require("fs");

const myPromise = new Promise((resolve, reject) => {
  fs.readFile("person.json", (encoding = "utf8"), (error, data) => {
    if (error) {
      reject("error");
    } else {
      resolve(JSON.parse(data));
    }
  });
});
myPromise
  .then((data) => {
    element = data.filter((element) => {
      if (element.age > 25) {
        element.firstName = element.firstName.toUpperCase();
        element.lastName = element.lastName.toUpperCase();
        return element;
      }
    });
    return element;
  })
  .then((data) => {
    fs.writeFile("promise.json", JSON.stringify(data), (err) => {
      console.log("error");
    });
  })
  .catch((err) => {
    console.log("err");
  });
