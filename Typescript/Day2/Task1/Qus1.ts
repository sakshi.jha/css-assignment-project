// Write the type of response getting from this url

// https://fakestoreapi.com/products/1

const callingApi = async () => {
  const api: string = "https://fakestoreapi.com/products/1";
  let data = await fetch(api);
  let response:
    | {
        id: number;
        title: string;
        price: number;
      }
    | {
        description: string;
        category: string;
        image: string;
        rating: {
          rate: number;
          count: number;
        };
      } = await data.json();

  console.log(response);
};

callingApi();
