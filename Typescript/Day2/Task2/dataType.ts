//Write a program to demonstrate all the data types in Typescript.
//Eg. number,string,boolean,object,array,tuple,enum,union types.

// number type
let temp: number = 20;
console.log(temp);

// string type
const day: string = "Monday";
console.log(day);

// boolean type
const flag: boolean = true;
console.log(flag);

//object type
const user: {
  name: string;
  username: string;
  admin: boolean;
} = {
  name: "sakshi",
  username: "sakshi123",
  admin: true,
};
console.log((user.name = "sakshi jha"));

// array type
const students: string[] = ["Sakshi", "Ritu", "Kajal", "Akshat"];
console.log(students[3]);

// TUPLE TYPE
let employee: [string, number, string] = [
  "Sakshi",
  1432,
  "Software Developer Intern",
];

//enum type
enum Status {
  Notfound = 404,
  Ok = 200,
  Forbidden = 401,
  BadRequest = 403,
  ServerError = 500,
}
let currentStatus: Status = Status.BadRequest;
console.log(`The current status of my server is ${currentStatus}`);

// union type
let citizen:
  | {
      name: string;
      aadhar: string;
      accountName: string;
      accountNumber: string;
      bankName: string;
      cardNo: number;
    }
  | {
      cardNo: string;
      cvv: number;
      cardIssuer: string;
    } = {
  name: "Karan",
  aadhar: "24334343434",
  accountName: "Karan Gupta",
  accountNumber: "34343434",
  bankName: "SBI",
  cardNo: "343434343434343",
  cvv: 343,
  cardIssuer: "VISA",
};

// string literal
let weekDay: "sunday" | "monday" | "tuesday" = "sunday";
weekDay = "tuesday";
