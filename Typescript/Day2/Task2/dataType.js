//Write a program to demonstrate all the data types in Typescript.
//Eg. number,string,boolean,object,array,tupple,enum,union types.
// number type
var temp = 20;
console.log(temp);
// string type
var day = "Monday";
console.log(day);
// boolean type
var flag = true;
console.log(flag);
//object type
var user = {
    name: "sakshi",
    username: "sakshi123",
    admin: true
};
console.log((user.name = "sakshi jha"));
// array type
var students = ["Sakshi", "Ritu", "Kajal", "Akshat"];
console.log(students[3]);
// TUPLE TYPE
var employee = [
    "Sakshi",
    1432,
    "Software Developer Intern",
];
//enum type
var Status;
(function (Status) {
    Status[Status["Notfound"] = 404] = "Notfound";
    Status[Status["Ok"] = 200] = "Ok";
    Status[Status["Forbidden"] = 401] = "Forbidden";
    Status[Status["BadRequest"] = 403] = "BadRequest";
    Status[Status["ServerError"] = 500] = "ServerError";
})(Status || (Status = {}));
var currentStatus = Status.BadRequest;
console.log("The current status of my server is ".concat(currentStatus));
