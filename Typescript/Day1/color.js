//The program will take color names as input and output a two digit number.
//The band colors are encoded as follows:
//From the example above: brown-green should return 15 brown-green-violet should return 15 too, ignoring the third color.
Black: 0;
Brown: 1;
Red: 2;
Orange: 3;
Yellow: 4;
Green: 5;
Blue: 6;
Violet: 7;
Grey: 8;
White: 9;
var color = {
    Black: 0,
    Brown: 1,
    Red: 2,
    Orange: 3,
    Yellow: 4,
    Green: 5,
    Blue: 6,
    Violet: 7,
    Grey: 8,
    White: 9
};
function keyValue(str1) {
    var s = str1.split("-");
    console.log("".concat(color[s[0]]).concat(color[s[1]]));
}
keyValue("Green-Violet-white");
