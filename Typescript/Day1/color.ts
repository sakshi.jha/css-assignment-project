//The program will take color names as input and output a two digit number.
//The band colors are encoded as follows:
//From the example above: brown-green should return 15 brown-green-violet should return 15 too, ignoring the third color.

const color: {
  Black: number;
  Brown: number;
  Red: number;
  Orange: number;
  Yellow: number;
  Green: number;
  Blue: number;
  Violet: number;
  Grey: number;
  White: number;
} = {
  Black: 0,
  Brown: 1,
  Red: 2,
  Orange: 3,
  Yellow: 4,
  Green: 5,
  Blue: 6,
  Violet: 7,
  Grey: 8,
  White: 9,
};

function bandColors(str1: string) {
  const s = str1.split("-");
  console.log(`${color[s[0]]}${color[s[1]]}`);
}

bandColors("Green-Violet-white");
